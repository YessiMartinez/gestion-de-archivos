#include <stdio.h>
#include <stdlib.h>
#include <string.h>

	char name_file[50];
	char name_file_out[50];
	char Word[90][50];


FILE *OpenFile();

		void CloseFile(FILE *ptr_Fich, char*name_file);
		
		int Val_exist(int argc, char *argv[]);
		
		int CreateWriteFile(char *name_file_create, int count);
		
		void ShowWord(int count);
		
		void BubbleSort(int count);
		
		void ChargeDataDat(FILE *ptr_file_Dat, int count);

int main(int argc, char *argv[]) {
	int exist_file = 0, count;
	

FILE *ptr_Fich = NULL;
	exist_file = Val_exist(argc, argv);
	
	if (exist_file == 0 ) {
    ptr_Fich = OpenFile();
    
    if (ptr_Fich != NULL) {
      printf("\n [ %s ]Abierto correctamente \n\n", name_file);
      
      count=0;
      while (feof(ptr_Fich) == 0) {
        fscanf(ptr_Fich, "%s", Word[count]);
        count++;
      }
      BubbleSort(count);
      
      CreateWriteFile(name_file_out, count);
      CloseFile(ptr_Fich,name_file);
    }else{
      printf("El fichero no existe\n", name_file);
    }
  }
  return 0;
}


int Val_exist(int argc, char *argv[]){
  int error = 0;
  
  if (argc == 1) {
    printf("Volver a ejecutar \n\n");
    error = 1;
    
  }else if(argc == 2){
    printf("Hacen falta datos \n\n");
    error = 2;
    
  }else{
    strcpy(name_file,argv[1]);
    strcpy(name_file_out,argv[2]);
    error = 0;
    
  }
  return error;
}


FILE *OpenFile(){
  printf("Nombre de archivo a leer: \n\n",name_file );
  
  FILE *file = fopen(name_file,"r");
  
  return file;
}


void CloseFile(FILE *ptr_Fich, char *name_file){
  fclose(ptr_Fich);
  
  printf("\n FICHERO CERRADO \n\n",name_file);
}


void BubbleSort(int count){
  char aux_word[15];
  
	for(int i = 0; i < count-1; i++){
		for(int j = 0; j < count-1; j++){
			if(Word[j][0] > Word[j+1][0]){
        strcpy(aux_word, Word[j]);
        strcpy(Word[j],Word[j+1]);
        strcpy(Word[j+1],aux_word);
			}
    }
	}
}

void ShowWord(int count){
  for (int i = 0; i < count; i++) {
    printf("[ %s ]\n",Word[i]);
  }
}


int CreateWriteFile(char *name_file_create, int count){
  int StatusCreate = 0;
  
  
  FILE *ptr_Fich_Create;
  ptr_Fich_Create = fopen(name_file_create,"w");
  if ( ptr_Fich_Create == NULL) {
    printf("Archvo no creado\n",name_file_create);
  }else{
    printf("\n Archivo creado \n",name_file_create);
    StatusCreate = 1;
    ChargeDataDat(ptr_Fich_Create, count);
    
    CloseFile(ptr_Fich_Create,name_file_out);
  }
  return StatusCreate;
}

void ChargeDataDat(FILE *ptr_file_Dat, int count){
  for (int i = 0; i < count; i++) {
    fputs(Word[i],ptr_file_Dat);
    
    fputs("\n",ptr_file_Dat);
    
  }
}
